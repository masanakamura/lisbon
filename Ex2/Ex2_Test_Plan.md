### Test Planning

Write a test plan for the following flow:

* Login
* Choose one task
* Perform annotation
* Take a break
* Resume annotation
* Finish annotation

# 0. History

  Version 1.0  2018-07-23   Initial version

# 1. Purpose & Scope

  To test the functional features from Login to finish annotation are work correctlly as specification
  In general using black-box testing method with presumpotion that no direct database access is available.
  
# 2. Environment & Tools

  Client side configuration:
  * Browser:  Google Chrome v67 
  * OS:   Mac OS 10.13

# 3. Effort Level & Schedule

  One day including report.

# 4. Output

  A report show  cases are successed or failed 

# 5. Test Cases
## 1. Normal Cases
### 1.1 Login
    * Prepare
        Regist a user for test purpose
    * Steps
        Open the login page in browser
        Input correct user name and password
        Click "Sign In"
    * Expected Result
        Login succeed 
        Task list Page is displayed
    * Result

### 1.2 Choose one task
    *  Steps
        After login, click on the language pair of a task
    *  Expected Result
        the annotation page of the task displayed
    *  Result

### 1.3 Perform annotation
    *  Steps
        On annotion page, select a text, Error Type and Severity
        click Add
    *  Expected Result
        A new annotation is added and displayed in the left pan. 
    *  Result

### 1.4 Take a Break
    *  Steps
        On annotation page, click the "Take a Break" 
    *  Expected Result
        The clock stop ticking
        The botton toggled to "Resume annotation"
    *  Result

### 1.5 Resume annotation
    *  Steps
        On annotation page, click the ""Resume annotation 
    *  Expected Result
        The clock resume ticking
        The botton toggled to "Take a Break"
    *  Result

### 1.6 Finish annotation
    *  Steps
      Click "+ Finish or Report" 
      Click Finish
      Click Task fluency
      Input Task comment
      Click "Finish"
      Click 'YES" on the dialog 
    *  Expected Result
      The task is finished and the page navigate to the next task 
    *  Result

## 2. Semi-Normal Cases
### 2.1 Login 
    *  Steps
        Open the login page in browser
        Input incorrect user name or incorrect password
        Click "Sign In"
    * Expected Result
        Login failed 
        Show a autentication error message
    *  Result


