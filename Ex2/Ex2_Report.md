### Test Execution and Reporting

Execute the previous test plan and create a simple report with it.

# 1. Summary
##  Tester
  Masa Nakamura

##  Test date
  2018-07-22

##  Test result:

  All the test cases listed in the Test Plan are passed except the issue below:

  * No "Take a break" button on annotation page

# 2. Issue Details
##   No "Take a break" button on annotation page 

  * Issue:

      There is no "Take a break" button in the annotation page 

  * Reproduce Steps:

      Login to the Annotation Tool
      Click on the Annotate tab

  * Expected Result

      There is a "Take a break" button in the Annotate section

  * Actual Result

      No "Take a break" button

  * Repeatbility:

       Always 


