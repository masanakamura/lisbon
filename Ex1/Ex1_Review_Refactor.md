---
### 1.1 Review and Refactor

Given the Annotation Guideline document available [here](https://drive.google.com/file/d/0B2jU6glQtwcpSS1PQ0g2NGdQNWM/view?usp=sharing) provide 5 specific examples of things you would change, why and how.

---

======

# 1.  Make existing annotations Selectrable and editable

  When click an existing annotation, the text higlisghted, Error Type and Serverity filled automaticlly. This would easier for a user to modify an existing annotation e.g. from Minor to Major.

# 2. Add a quick templates for effceincy

  In many cases the Error Type and Serverity are the same and frequent, and a user can  pre-define a few quick templates(error types), so a user just highlight the text and select a quick template to speed up the annotation process

# 3. Update the "How to navigate between tasks" contents

  The content of  "How to navigate between tasks" looks old, there are a line of small circle in the top center after selected a language pair, when this task finished or reported, the circle became filled. 

# 4. Add the conents on how to use the Propose Glossary Terms span 

  There is no guideline or explanation on how to use the Propose Glossary Terms, would be better to add the guideline  

# 5. Add a password reset feature

  Some time user forgot their password, add a password rest link would be useful.


