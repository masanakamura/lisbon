---
### 1.2 Spec out

Write the Functional Analysis spec that would be given to a developer for the login/signup page and mechanism.


# 1. Scope  

This document describes the functional requirements for the login page of Annotation Tool.

# 2. Browser supported
 Microsoft Edge, Firefox, Chrome and  Safari need be supported
  
# 3. Requirements 

The following sub-sections delineate the major requirements for Hierarchical Authentication system.  

## 3.1 login Page
The following parts are needed for the login page.

* Annoate tool logo (prefer on the left upper part of the page)
* username and password input area in the middle of the page 
* "Sign In" button for user to submit credential info
  
## 3.2 login mechanism
  
The server shall contains database which define user names, passwords and access roles, e.g. Administrator role, User role. 
These definitions  shall be inaccessible from the client or other illegal remote access in any form. 

In case of a successful logon, the proper page shall be sent to the client allowing the displayed information to be read,  
In case of a failure, a default authentication page shall be displayed with a notification "Warning! Invalid Credentials. Please try again." 
  
## 3.3 Logout  
After a successful login and the login page is displayed, Main Page shall contain a logout control.  all subordinate windows are closed.  
  
## 3.4  Contacts 
Have a contact therfore a user can  access in case password forgot etc.
  
  
  
  
