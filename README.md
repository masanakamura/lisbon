# QA Engineer Challenge

This QA Engineering challenge was created to give us a sense of how you'll operate in a real product scenario at Unbabel.

The challenge is divided into 3 areas, Requirement Analysis, Functional Testing and Non-functional Testing.

Each exercise is focused on a QA specific area and you should respect the existing estimated time.

The following exercises will be centered on an existing Unbabel tool called ***Annotation tool***.

You should receive a demo of this tool, as well as the url and valid credentials, before executing the exercises. If you haven't, request them.

***Important Notes:***

* Create a public repo in your github/gitlab account and send it to us right away
* Create one folder in that repo per exercise
* ***Exercise 1 *** should be written in Markdown format (md)
* ***Exercise 2 and 3*** can be submitted in whichever form you prefer as long as you also submit a README.md file alongside it with detailed instructions on how to run your solution
* We'll check your progress periodically
* If you have any questions, please ask.


## 1. Requirement Analysis
Estimated time: 1 day (off hours only).

### 1.1 Review and Refactor

Given the Annotation Guideline document available [here](https://drive.google.com/file/d/0B2jU6glQtwcpSS1PQ0g2NGdQNWM/view?usp=sharing) provide 5 specific examples of things you would change, why and how.

### 1.2 Spec out

Write the Functional Analysis spec that would be given to a developer for the login/signup page and mechanism.

## 2. Functional Testing
Estimated time: 2 days (off hours only).

### Test Planning

Write a test plan for the following flow: 

* Login
* Choose one task
* Perform annotation
* Take a break
* Resume annotation
* Finish annotation

### Test Execution and Reporting

Execute the previous test plan and create a simple report with it.

## 3. Non-Functional Testing
Estimated time: 3 days (off hours only).

### Automation

Choose a platform/framework of your liking (as long as it is free or open source) and automate the following:

* Login
* Choose one task
* Perform annotation
* Finish annotation

### Security

* Analyse this platform and tell us if there's something you'd change in terms of security measures.


### Load Testing

It's expected that this service must support 75 users at any given moment and must be able to handle 350 requests per second. The response time must be under 200 ms.  

* Perform a load test on the service and present a small report with your perspective on the test results:
* The URL will be provided by email.

Find below the service documentation.



## API documentation

* **URL**
    /return_eu_countries

* **Method:**
    `POST` 

* **Description**
The basic functionality of this service is to receive a list of iso country codes and return if there are any EU countries on that list.
The iso codes of the European Union countries are listed below:

|	Country	        |	ISO	|
|	-----	        |	--	|
|	Belgium    	    |	BE	|
|	Greece  	    |	EL	|
|	Lithuania 	    |	LT	|
|	Portugal 	    |	PT	|
|	Bulgaria	    |	BG	|
|	Spain 	        |	ES	|
|	Luxembourg      |	LU	|
|	Romania 	    |	RO	|
|	Czech Republic  |	CZ	|
|	France 	        |	FR	|
|	Hungary	        |	HU	|
|	Slovenia	    |	SI	|
|	Denmark	        |	DK	|
|	Croatia	        |	HR	|
|	Malta	        |	MT	|
|	Slovakia	    |	SK	|
|	Germany	        |	DE	|
|	Italy	        |	IT	|
|	Netherlands	    |	NL	|
|	Finland		    |   FI	|




* **Data Params**

    |Name          | Type | Description                               | Example                                     |
    |--------------|------|-------------------------------------------|---------------------------------------------|
    |country_list  |Array |Array of elements to compare with Source   | {"country_list": ["BE", "DE", "NY"]}        |





* **Success Response:**
  
    **Code:** 200 <br />
    **Content:** `{"value":"Match","status":"OK","result":["BE","DE"]}`

    **Code:** 200 <br />
    **Content:** `{"value":"No match","status":"OK","result":[]}`



 
* **Error Response:**
  * **Code:** 400 <br />
  *  **Content:** `{"request_sent":{"county_list":["PT"]},"error":"No country_list on request"}`





* **Sample Call:**
    * Request:
    
    ```
    curl -X POST \
      http://{{URL}}/api/return_eu_countries \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \
      -d '{"country_list": ["BE", "DE", "NY"]}'
    ```
   * Response:
   
    ```
    {"value":"Match","status":"OK","result":["BE","DE"]}
    ```