### Security

* Analyse this platform and tell us if there's something you'd change in terms of security measures.


## 1. Add anti-bots measure
Possible can add anti-bots measure - e.g. reCAPTCHA or similar configuration so as to prevent possible botnet's attempt to guess user credentials.


## 2. Server version
Better to config the server to avoid server type and version in the http response header

```
   Server: nginx/1.13.9 
```
