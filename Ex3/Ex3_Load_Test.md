### Load Testing

It's expected that this service must support 75 users at any given moment and must be able to handle 350 requests per second. The response time must be under 200 ms.

* Perform a load test on the service and present a small report with your perspective on the test results:
* The URL will be provided by email.

Find below the service documentation.


## 1. Prepare
Run  jmeter and Load performance_test.jmx configuration

## 2. Run 75 users test

At first run with 75 users by config  "Number of Threads(users) = 75", the Summary Report is:

'''
Label,# Samples,Average,Min,Max,Std. Dev.,Error %,Throughput,Received KB/sec,Sent KB/sec,Avg. Bytes
HTTP Request,75,636,629,753,15.96,0.000%,7.13470,2.61,1.78,374.0
TOTAL,75,636,629,753,15.96,0.000%,7.13470,2.61,1.78,374.0
'''

Conclusion

* No error
* Avg Response Time is 636ms, Max Response time is 753 ms
* This is doesn't meet the 200ms response requirement however this is depend on access location 

## 3. Run 350 users test
Now run with 350 users by config  "Number of Threads(users) = 350", the Summary Report is:

'''
Label,# Samples,Average,Min,Max,Std. Dev.,Error %,Throughput,Received KB/sec,Sent KB/sec,Avg. Bytes
HTTP Request,350,640,630,701,10.83,0.000%,32.77767,11.97,8.19,374.0
TOTAL,350,640,630,701,10.83,0.000%,32.77767,11.97,8.19,374.0
'''

Conclusion

* No Error
* Avg Response Time is 640ms, Max Response time is 701 ms 
* Again this is doesn't meet the 200ms response requirement however this is depend on access location 
