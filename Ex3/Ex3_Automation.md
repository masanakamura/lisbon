Choose a platform/framework of your liking (as long as it is free or open source) and automate the following:

Login
Choose one task
Perform annotation
Finish annotation

# 1. Prepare

## 1.1 Install selenium 
pip install selenium

## 1.2 Install chromedriver
Donwload chromedriver and put under /usr/local/bin/


# 2. Run test

python annotation_test_chrome.py

